package edu.ntnu.idatt1002.k1g02;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This class starts the application
 *
 * @author Kohort 1 Group 2 IDATT1002
 * @version 1.0
 */
public class App extends Application {
    private static Scene scene;
    private static TaskRegister taskRegister;

    /**
     * Sets the root of the scene.
     *
     * @param fxml the name of the fxml file to set as a root.
     * @throws IOException if the fxml file is not found.
     */
    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Loads in the fxml file from a given string of the name.
     *
     * @param fxml the name of the fxml file to find and return.
     * @return the fxml file loaded.
     * @throws IOException if the file isn't found.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

    public static TaskRegister getTaskRegister() {
        return taskRegister;
    }

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 640, 480);
        stage.setScene(scene);
        stage.getIcons().add(new Image("file:/favicon.png"));
        stage.setMinWidth(800);
        stage.setMinHeight(650);
        stage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
        taskRegister = new TaskRegister();
    }
}