package edu.ntnu.idatt1002.k1g02;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;

import java.time.LocalDate;

public class Task {
    final int UNIQUE_ID;
    String name;
    String description;
    String category;
    LocalDate dueDate;
    String priority;
    boolean isFinished;
    LocalDate startDate;
    LocalDate finishDate;

    /**
     * Constructor containing the properties in the Task class except for dueDate.
     */
    public Task(String name, String description, String category, String priority, int uniqueId) {
        if (description.isEmpty()) {
            description = " ";
        }
        this.isFinished = false;
        this.name = name;
        this.description = description;
        this.category = category;
        this.priority = priority;
        this.UNIQUE_ID = uniqueId;
        this.startDate = LocalDate.now();
    }

    /**
     * Constructor containing all properties of the Task class.
     */

    public Task(String name, String description, String category, LocalDate dueDate, String priority, LocalDate startDate, boolean isFinished, LocalDate finishDate, int UNIQUE_ID) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.dueDate = dueDate;
        this.priority = priority;
        this.UNIQUE_ID = UNIQUE_ID;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.isFinished = isFinished;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public void setFinishDate(LocalDate finishedDate) {
        this.finishDate = finishedDate;
    }

    public boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    public int getUNIQUE_ID() {
        return UNIQUE_ID;
    }

    public String convertToCSV() {
        if (isFinished) {
            // use isFinished instead of a string value when printing isFinished's value, in case of later changes
            return name + ";" + dueDate.toString() + ";" + description + ";" + priority + ";" + category + ";" + isFinished + ";"
                    + startDate.toString() + ";" + finishDate.toString();
        } else {
            return name + ";" + dueDate.toString() + ";" + description + ";" + priority + ";" + category + ";" + isFinished + ";"
                    + startDate.toString() + ";" + null;
        }

    }

    /**
     * Sets up the task as an HBox showing name, due date and category
     * Id of HBox is set to be the same as the unique Id for the task.
     *
     * @return the task formatted as an HBox to be used in the UI
     */
    public HBox getAsHBox() {
        HBox hBox = new HBox();
        hBox.setId("" + UNIQUE_ID);
        hBox.setPadding(new Insets(5, 5, 5, 5));
        hBox.setAlignment(Pos.CENTER);

        // Auto-resizing spacer
        Region spacer = new Region();
        Region spacer2 = new Region();
        Region spacer3 = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        HBox.setHgrow(spacer2, Priority.ALWAYS);
        HBox.setHgrow(spacer3, Priority.ALWAYS);

        // Static text to use
        Text priority = new Text(this.priority);
        Text name = new Text(this.name);
        Text dueDate = new Text("Due: " + this.dueDate.toString());
        Text category = new Text("Category: " + this.category.toUpperCase());

        HBox priorityBox = new HBox(priority);
        priorityBox.setPadding(new Insets(0, 5, 0, 0));
        priorityBox.setAlignment(Pos.CENTER);
        HBox nameBox = new HBox(name);
        nameBox.setAlignment(Pos.CENTER_LEFT);
        HBox dueDateBox = new HBox(dueDate);
        dueDateBox.setAlignment(Pos.CENTER_LEFT);
        HBox categoryBox = new HBox(category);
        categoryBox.setAlignment(Pos.CENTER_LEFT);

        priorityBox.setMaxWidth(15);
        nameBox.setMaxWidth(70);
        dueDateBox.setMaxWidth(100);
        categoryBox.setMaxWidth(100);
        priorityBox.setMinWidth(15);
        nameBox.setMinWidth(70);
        dueDateBox.setMinWidth(100);
        categoryBox.setMinWidth(100);

        // Adds all elements to the HBox, and returns it
        hBox.getChildren().addAll(priorityBox, nameBox, spacer, dueDateBox, spacer2, categoryBox, spacer3);
        return hBox;
    }

    /**
     * Mark the task as finished.
     * When method is called, finish date is set to the date when method is called and
     * boolean getIsFinished is set to "true"
     */
    public void finishTask() {
        setIsFinished(true);
        setFinishDate(LocalDate.now());
    }

    /**
     * Un-marks the task as finished
     * When method is called, finish date is removed and boolean getIsFinished is set to "false"
     */
    public void notFinished() {
        setIsFinished(false);
        setFinishDate(null);
    }

    /**
     * Check to see if two tasks are equal
     *
     * @param o object to check against
     * @return true if uniqueId of task passed to the method is equal to this.uniqueId
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return UNIQUE_ID == task.UNIQUE_ID;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", dueDate=" + dueDate +
                ", priority=" + priority +
                ", uniqueId=" + UNIQUE_ID +
                '}';
    }
}