package edu.ntnu.idatt1002.k1g02.dao;

import edu.ntnu.idatt1002.k1g02.Task;
import edu.ntnu.idatt1002.k1g02.TaskRegister;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

public class TaskDAO {
    public final String DELIMITER = ";";

    public TaskDAO() { }

    /**
     * Uses FileInputStream to read tasks from CSV file and adds them to the register.
     * Uses try with resource to automatically close the connection if it fails.
     * Prints out the stack trace if IOException is thrown.
     * If task is not finished, getIsFinished is set to false and finishDate is set to null.
     * @param taskRegister register to add tasks to
     */
    public void fillRegisterFromCSV(TaskRegister taskRegister) {
        String line;
        try (FileInputStream fileInputStream = new FileInputStream(taskRegister.getSaveFile());
            InputStreamReader isr = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr)) {
            while ((line = br.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] task = line.split(DELIMITER);
                    boolean isFinished = task[5].equals("true");
                    if (task[7].equals("null")) {
                        taskRegister.addTask(task[0], task[2], task[4], LocalDate.parse(task[1]), task[3], LocalDate.parse(task[6]), isFinished, null);
                    } else {
                        taskRegister.addTask(task[0], task[2], task[4], LocalDate.parse(task[1]), task[3], LocalDate.parse(task[6]), isFinished, LocalDate.parse(task[7]));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses OutputStream and convertToCSV to save tasks to CSV file.
     * Uses try with resource to automatically close the connection if it fails.
     * Prints out the stack trace if IOException is thrown.
     * @param taskRegister register to be saved
     */
    public void saveToCSV(TaskRegister taskRegister) {
        try (OutputStream os = new FileOutputStream(taskRegister.getSaveFile());
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            taskRegister.getTasks().stream().map(Task::convertToCSV).forEach(pw::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
