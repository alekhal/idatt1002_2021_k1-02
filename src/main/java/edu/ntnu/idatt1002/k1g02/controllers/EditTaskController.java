package edu.ntnu.idatt1002.k1g02.controllers;

import edu.ntnu.idatt1002.k1g02.App;
import edu.ntnu.idatt1002.k1g02.Task;
import edu.ntnu.idatt1002.k1g02.TaskRegister;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Window;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Controller class for the edit task window.
 */
public class EditTaskController {

    final String[] priorityNumber = {"!", "!!", "!!!"};
    @FXML
    TextField taskName;
    @FXML
    TextField category;
    @FXML
    ComboBox<String> priority;
    @FXML
    TextArea description;
    @FXML
    DatePicker dueDate;
    private Task currentTask;
    @FXML
    private Button confirmBtn;
    @FXML
    private Button completeBtn;
    @FXML
    private Button deleteBtn;

    /**
     * Opens a task to work with from the given id.
     *
     * @param id unique id of the task to open.
     */
    public void initializeEdit(int id) {

        currentTask = App.getTaskRegister().getTaskFromUniqueId(id);
        taskName.setText(currentTask.getName());
        taskName.setOnKeyTyped(e -> {
            if (taskName.getText().length() > 40) {
                Alert maxCharactersReached = new Alert(Alert.AlertType.INFORMATION);
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Task name too long!");
                maxCharactersReached.setContentText("Task name can only be 40 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        dueDate.setValue(currentTask.getDueDate());
        category.setText(currentTask.getCategory());
        category.setOnKeyTyped(e -> {
            if (category.getText().length() > 25) {
                Alert maxCharactersReached = new Alert(Alert.AlertType.WARNING);
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Category name too long");
                maxCharactersReached.setContentText("Category name can only be 25 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        description.setText(currentTask.getDescription());
        description.setWrapText(true);
        priority.getItems().setAll(FXCollections.observableArrayList(priorityNumber));
        priority.setValue(currentTask.getPriority());

        confirmBtn.setOnAction(e -> setTask(currentTask));
        deleteBtn.setOnAction(e -> deleteTask(App.getTaskRegister(), currentTask));
        completeBtn.setOnAction(e -> completeTask(currentTask));
    }

    /**
     * Edits the information from the task to the new values, then closes the window.
     *
     * @param currentTask the task that is opened.
     */
    private void setTask(Task currentTask) {
        if (taskName.getText().isEmpty() || category.getText().isEmpty() || dueDate.getValue() == null || priority.getValue() == null) {
            System.out.println("Something missing in edited task");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Some elements of the task is missing. \nTitle, due date, category and priority are all requiered fields");
            alert.setTitle("Something is missing");

            alert.showAndWait();
        } else {
            currentTask.setName(taskName.getText());
            currentTask.setDescription(description.getText());
            currentTask.setDueDate(dueDate.getValue());
            currentTask.setPriority(priority.getValue());
            currentTask.setCategory(category.getText());
            close();
        }
    }

    /**
     * Sets the task to finished-status. Changes the finish date to the current date. Closes the window.
     *
     * @param currentTask the task to finish.
     */
    private void completeTask(Task currentTask) {
        currentTask.setIsFinished(true);
        currentTask.setFinishDate(LocalDate.now());
        close();
    }

    /**
     * Deletes the current task from the register. Closes the window after.
     *
     * @param register    the register to delete from.
     * @param currentTask the task to remove.
     */
    private void deleteTask(TaskRegister register, Task currentTask) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Delete");
        alert.setHeaderText("Are you sure you want to delete task?");
        alert.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> showAlert = alert.showAndWait();
        ButtonType result = showAlert.orElse(ButtonType.CANCEL);
        if (result == ButtonType.OK){
            register.removeTask(currentTask.getUNIQUE_ID());
            close();
        }
    }

    /**
     * Closes the window.
     */
    private void close() {
        Window window = confirmBtn.getScene().getWindow();
        window.hide();
    }
}
