package edu.ntnu.idatt1002.k1g02.controllers;

import edu.ntnu.idatt1002.k1g02.App;
import edu.ntnu.idatt1002.k1g02.TaskRegister;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Window;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Controller class for the add task window.
 */
public class AddTaskController implements Initializable {

    final String[] priorityNumber = {"!", "!!", "!!!"};
    @FXML
    TextField taskName;
    @FXML
    TextField category;
    @FXML
    ComboBox<String> priority;
    @FXML
    TextArea description;
    @FXML
    DatePicker dueDate;
    private TaskRegister register;
    @FXML
    private Button confirmBtn;
    @FXML
    private Button cancelBtn;

    /**
     * Ran after root element is completely processed (the fxml file).
     *
     * @param url            the location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle the resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        register = App.getTaskRegister();
        priority.getItems().setAll(FXCollections.observableArrayList(priorityNumber));
        confirmBtn.setOnAction(event -> addTask(register));
        cancelBtn.setOnAction(event -> close());
        taskName.setOnKeyTyped(e -> {
            if (taskName.getText().length() > 40) {
                Alert maxCharactersReached = new Alert(Alert.AlertType.WARNING);
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Task name too long!");
                maxCharactersReached.setContentText("Task name can only be 40 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        category.setOnKeyTyped(e -> {
            if (category.getText().length() > 25) {
                Alert maxCharactersReached = new Alert(Alert.AlertType.WARNING);
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Category name too long");
                maxCharactersReached.setContentText("Category name can only be 25 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        description.setWrapText(true);
    }

    /**
     * Method to add a task to the register with the given information. If any information is missing, an alert will pop up telling you.
     * When completed, the window closes.
     *
     * @param register the task register to add the task to.
     */
    @FXML
    private void addTask(TaskRegister register) {
        if (taskName.getText().isEmpty() || category.getText().isEmpty() || dueDate.getValue() == null || priority.getValue() == null) {
            System.out.println("Something missing in added task");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Some elements of the task is missing. \nTitle, due date, category and priority are all required fields");
            alert.setTitle("Something is missing");

            alert.showAndWait();
        } else {
            register.addTask(taskName.getText(), description.getText(), category.getText(), dueDate.getValue(), priority.getValue(), LocalDate.now(), false, null);
            close();
        }
    }

    /**
     * Closes the window when called.
     */
    private void close() {
        Window window = confirmBtn.getScene().getWindow();
        window.hide();
    }

}
