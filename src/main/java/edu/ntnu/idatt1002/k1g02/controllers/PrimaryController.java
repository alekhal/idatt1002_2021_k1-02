package edu.ntnu.idatt1002.k1g02.controllers;

import edu.ntnu.idatt1002.k1g02.App;
import edu.ntnu.idatt1002.k1g02.Task;
import edu.ntnu.idatt1002.k1g02.TaskRegister;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Controller class for the main window.
 */
public class PrimaryController implements Initializable {
    @FXML
    public ToggleGroup sortBy;
    public VBox taskVBox;
    private TaskRegister taskRegister;

    /**
     * Called to initialize the controller after the root element has been completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resources The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.taskRegister = App.getTaskRegister();
        taskRegister.load();
        sortBy.getToggles().get(1).setSelected(true);
        showTasksSortedByDueDate();
    }

    /**
     * Handler for clicking a sort button. Sets the corresponding button to toggled, un-toggles the others
     * and shows the tasks sorted after the corresponding rules.
     */
    @FXML
    public void sortByPriorityButton() {
        sortBy.getToggles().get(0).setSelected(true);
        showTasksSortedByPriority();
        refreshView();
    }

    /**
     * Handler for clicking a sort button. Sets the corresponding button to toggled, un-toggles the others
     * and shows the tasks sorted after the corresponding rules.
     */
    @FXML
    public void sortByDueDateButton() {
        sortBy.getToggles().get(1).setSelected(true);
        showTasksSortedByDueDate();
    }

    /**
     * Handler for clicking a sort button. Sets the corresponding button to toggled, un-toggles the others
     * and shows the tasks sorted after the corresponding rules.
     */
    @FXML
    public void sortByCategoryButton() {
        sortBy.getToggles().get(2).setSelected(true);
        showTasksSortedByCategory();
    }

    /**
     * Handler for clicking a sort button. Sets the corresponding button to toggled, un-toggles the others
     * and shows the tasks marked as finished.
     */
    @FXML
    public void sortByFinishedButton() {
        sortBy.getToggles().get(3).setSelected(true);
        showFinishedTasks();
    }

    /**
     * Handler for the "add task"-buttons. Opens a new window where you can enter task information, confirm adding the
     * task, cancel adding the task etc. Locks the program so you must close the new window before doing anything else.
     */
    @FXML
    public void addTaskButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/addTask.fxml"));
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            taskRegister.save();
            refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the edit task window with the selected tasks opened.
     *
     * @param currentId the id of the task you want to edit.
     */
    private void editTask(int currentId) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/editTask.fxml"));
            Parent root = fxmlLoader.load();

            EditTaskController editTaskController = fxmlLoader.getController();

            editTaskController.initializeEdit(currentId);

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            taskRegister.save();
            refreshView();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to create and return an arraylist containing arraylists of all different categories in the task register
     * and all corresponding tasks to that arraylist.
     *
     * @return an arraylist containing arraylists for every category containing the tasks that correspond to that category
     */
    private ArrayList<ArrayList<Task>> getCategoriesWithTaskSortedByCategories() {
        taskRegister.sortByCategory();
        List<String> uniqueCategories = taskRegister.getUnfinishedTasks().stream().map(e -> e.getCategory().toLowerCase(Locale.ROOT)).distinct().collect(Collectors.toList());
        ArrayList<ArrayList<Task>> categoriesWithTasks = new ArrayList<>();
        List<Task> subList1;
        for (int i = 0; i < uniqueCategories.size(); i++) {
            int check = i;
            subList1 = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getCategory().equalsIgnoreCase(uniqueCategories.get(check))).collect(Collectors.toList());
            categoriesWithTasks.add((ArrayList<Task>) subList1);
        }
        return categoriesWithTasks;
    }

    /**
     * Method to create and return an arraylist containing arraylists of all different priorities in the task register,
     * and all corresponding tasks added to that arraylist.
     *
     * @return an arraylist containing arraylists for every priority containing the tasks that correspond to that priority.
     */
    private ArrayList<ArrayList<Task>> getCategoriesWithTaskSortedByPriority() {
        taskRegister.sortByDate();
        taskRegister.sortByPriority();
        ArrayList<ArrayList<Task>> categoriesWithTasks = new ArrayList<>();
        List<Task> priority1;
        List<Task> priority2;
        List<Task> priority3;
        priority1 = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getPriority().equals("!!!")).collect(Collectors.toList());
        priority2 = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getPriority().equals("!!")).collect(Collectors.toList());
        priority3 = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getPriority().equals("!")).collect(Collectors.toList());
        if (!priority1.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) priority1);
        }
        if (!priority2.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) priority2);
        }
        if (!priority3.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) priority3);
        }
        return categoriesWithTasks;
    }

    /**
     * Method to create and return an arraylist containing arraylists of three different categories, today, tomorrow and future,
     * in the task register,
     * and all corresponding tasks added to that arraylist.
     *
     * @return an arraylist containing arraylists for "today", "tomorrow" and "future", containing the tasks that
     * correspond to each title.
     */
    private ArrayList<ArrayList<Task>> getCategoriesWithTasksSortedByDueDate() {
        taskRegister.sortByDate();
        ArrayList<ArrayList<Task>> categoriesWithTasks = new ArrayList<>();
        List<Task> today;
        List<Task> tomorrow;
        List<Task> future;
        today = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getDueDate().isBefore(LocalDate.now().plusDays(1))).collect(Collectors.toList());
        future = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getDueDate().isAfter(LocalDate.now().plusDays(1))).collect(Collectors.toList());
        tomorrow = taskRegister.getUnfinishedTasks().stream().filter(e -> e.getDueDate().equals(LocalDate.now().plusDays(1))).collect(Collectors.toList());

        if (!today.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) today);
        }
        if (!tomorrow.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) tomorrow);
        }
        if (!future.isEmpty()) {
            categoriesWithTasks.add((ArrayList<Task>) future);
        }
        return categoriesWithTasks;
    }

    /**
     * Adds all tasks as a hbox to the main window in the correct category-vbox
     */
    private void showTasksSortedByCategory() {
        taskVBox.getChildren().removeAll(taskVBox.getChildren());
        if (!taskRegister.getTasks().isEmpty()) {
            ArrayList<ArrayList<Task>> categories = getCategoriesWithTaskSortedByCategories();

            for (ArrayList<Task> tasks : categories) {
                VBox category = new VBox();

                Text categoryText = new Text(tasks.get(0).getCategory());
                HBox categoryTextBox = new HBox();
                categoryTextBox.setMaxWidth(0);
                categoryTextBox.setPadding(new Insets(1.5, 5, 1.5, 5));
                categoryTextBox.getChildren().add(categoryText);
                Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                Border focusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                categoryTextBox.borderProperty().bind(Bindings.when(categoryTextBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));

                category.getChildren().add(categoryTextBox);
                for (Task task : tasks) {
                    HBox taskHBox = setUpTaskHBox(task);
                    category.getChildren().add(taskHBox);
                }

                HBox addTaskHBox = addTaskHBox();
                category.getChildren().add(addTaskHBox);
                category.setSpacing(2);
                taskVBox.getChildren().add(category);
            }
        } else {
            HBox welcomeHBox = new HBox();
            welcomeHBox.setAlignment(Pos.CENTER);
            Text welcomeText = new Text("No task added, press Add Task to start using the app");
            welcomeHBox.getChildren().add(welcomeText);
            taskVBox.getChildren().add(welcomeHBox);
        }
    }

    /**
     * Adds all tasks as a hbox to the main window in the correct category-vbox
     */

    private void showTasksSortedByPriority() {
        taskVBox.getChildren().removeAll(taskVBox.getChildren());
        if (!taskRegister.getTasks().isEmpty()) {
            ArrayList<ArrayList<Task>> categories = getCategoriesWithTaskSortedByPriority();

            for (ArrayList<Task> tasks : categories) {
                VBox priority = new VBox();

                Text priorityText = new Text(tasks.get(0).getPriority());
                HBox priorityTextBox = new HBox();
                priorityTextBox.setMaxWidth(0);
                priorityTextBox.setPadding(new Insets(1.5, 5, 1.5, 5));
                priorityTextBox.getChildren().add(priorityText);
                Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                Border focusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                priorityTextBox.borderProperty().bind(Bindings.when(priorityTextBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));

                priority.getChildren().add(priorityTextBox);
                for (Task task : tasks) {
                    HBox taskHBox = setUpTaskHBox(task);
                    priority.getChildren().add(taskHBox);
                }

                HBox addTaskHBox = addTaskHBox();
                priority.getChildren().add(addTaskHBox);
                priority.setSpacing(2);
                taskVBox.getChildren().add(priority);
            }
        } else {
            HBox welcomeHBox = new HBox();
            welcomeHBox.setAlignment(Pos.CENTER);
            Text welcomeText = new Text("No task added, press Add Task to start using the app");
            welcomeHBox.getChildren().add(welcomeText);
            taskVBox.getChildren().add(welcomeHBox);
        }
    }

    /**
     * Adds all tasks as a hbox to the main window in the correct category-vbox
     */
    private void showTasksSortedByDueDate() {
        taskVBox.getChildren().removeAll(taskVBox.getChildren());
        if (!taskRegister.getTasks().isEmpty()) {
            ArrayList<ArrayList<Task>> categories = getCategoriesWithTasksSortedByDueDate();
            String headText;

            for (ArrayList<Task> category : categories) {
                VBox dueDate = new VBox();

                if (category.get(0).getDueDate().equals(LocalDate.now().plusDays(1))) {
                    headText = "Tomorrow";
                } else if (category.get(0).getDueDate().isAfter(LocalDate.now().plusDays(1))) {
                    headText = "Future";
                } else {
                    headText = "Today";
                }

                Text text = new Text(headText);
                HBox headTextBox = new HBox();
                headTextBox.getChildren().add(text);
                headTextBox.setMaxWidth(0);
                headTextBox.setPadding(new Insets(1.5, 5, 1.5, 5));

                Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                Border focusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
                headTextBox.borderProperty().bind(Bindings.when(headTextBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));

                dueDate.getChildren().add(headTextBox);

                for (Task task : category) {
                    HBox taskHBox = setUpTaskHBox(task);
                    dueDate.getChildren().add(taskHBox);
                }

                HBox addTaskHBox = addTaskHBox();
                dueDate.getChildren().add(addTaskHBox);
                dueDate.setSpacing(2);
                taskVBox.getChildren().add(dueDate);
            }
        } else {
            HBox welcomeHBox = new HBox();
            welcomeHBox.setAlignment(Pos.CENTER);
            Text welcomeText = new Text("No task added, press Add Task to start using the app");
            welcomeHBox.getChildren().add(welcomeText);
            taskVBox.getChildren().add(welcomeHBox);
        }

    }

    /**
     * Adds all tasks as a hbox to the main window in the correct category-vbox
     */
    private void showFinishedTasks() {
        taskVBox.getChildren().removeAll(taskVBox.getChildren());
        VBox finishedTasks = new VBox();
        ArrayList<Task> tasks = taskRegister.getFinishedTasks();
        for (Task task : tasks) {
            HBox taskHBox = task.getAsHBox();
            Button unFinishButton = new Button("Undo");
            unFinishButton.setOnAction(e -> {
                task.notFinished();
                taskRegister.save();
                refreshView();
            });
            taskHBox.getChildren().add(unFinishButton);
            Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));
            Border focusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(3)));

            taskHBox.setOnMouseClicked(e -> taskHBox.requestFocus());
            taskHBox.borderProperty().bind(Bindings.when(taskHBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));
            finishedTasks.getChildren().add(taskHBox);
        }
        finishedTasks.setSpacing(2);
        taskVBox.getChildren().add(finishedTasks);
    }

    /**
     * Creates Hbox with functionality to add new task.
     *
     * @return HBox with text and button to add new task
     */
    private HBox addTaskHBox() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(2)));
        Border focusedBorder = new Border(new BorderStroke(Color.DARKGRAY.darker(), BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(2)));

        Button addNewTaskButton = new Button("+");
        addNewTaskButton.setMinWidth(29);
        addNewTaskButton.setOnAction(e -> addTaskButton());

        HBox addTaskHBox = new HBox();
        Text text = new Text("Add new task...");
        text.setFont(new Font(13));
        addTaskHBox.getChildren().addAll(text, spacer, addNewTaskButton);
        addTaskHBox.setOnMouseClicked(e -> {
            addTaskHBox.requestFocus();
            if (e.getClickCount() == 2) {
                addTaskButton();
            }
        });
        addTaskHBox.borderProperty().bind(Bindings.when(addTaskHBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));

        addTaskHBox.setPadding(new Insets(5, 5, 5, 5));
        addTaskHBox.setAlignment(Pos.CENTER);
        return addTaskHBox;
    }

    /**
     * Sets up functionality for the task-hboxes
     *
     * @param task the task to make an hbox out of
     * @return the task represented by an hbox
     */
    private HBox setUpTaskHBox(Task task) {
        HBox taskHBox = task.getAsHBox();
        Button finishTaskButton = new Button("✓");
        finishTaskButton.setMinWidth(29);
        finishTaskButton.setOnAction(e -> {
                    task.finishTask();
                    taskRegister.save();
                    refreshView();
                }
        );
        taskHBox.getChildren().add(finishTaskButton);
        Border unfocusedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(2)));
        Border focusedBorder = new Border(new BorderStroke(Color.DARKGRAY.darker(), BorderStrokeStyle.SOLID, new CornerRadii(3), new BorderWidths(2)));

        taskHBox.setOnMouseClicked(e -> {
            taskHBox.requestFocus();
            if (e.getClickCount() == 2) {
                editTask(Integer.parseInt(taskHBox.getId()));
            }
        });
        taskHBox.borderProperty().bind(Bindings.when(taskHBox.focusedProperty()).then(focusedBorder).otherwise(unfocusedBorder));
        taskHBox.setStyle("-fx-background-color: #dcdcdc;");

        return taskHBox;
    }

    /**
     * Reloads the task-hboxes into the main window when there has been a change.
     */
    private void refreshView() {
        ToggleButton selectedView = (ToggleButton) sortBy.getSelectedToggle();
        switch (selectedView.getText()) {
            case "Priority":
                showTasksSortedByPriority();
                break;
            case "Due date":
                showTasksSortedByDueDate();
                break;
            case "Category":
                showTasksSortedByCategory();
                break;
            case "Finished":
                showFinishedTasks();
                break;
            default:
                System.out.println("Something went wrong");
        }
    }

    /**
     * Method to open a hyperlink to our central repo in the default browser
     */
    public void openHelp() {
        String url = "https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02/-/wikis/home";
        String os = System.getProperty("os.name").toLowerCase();
        Runtime rt = Runtime.getRuntime();

        try {

            if (os.contains("win")) {

                // this doesn't support showing urls in the form of "page.html#nameLink"
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);

            } else if (os.contains("mac")) {

                rt.exec("open " + url);

            } else if (os.contains("nix") || os.contains("nux")) {

                // Do a best guess on unix until we get a platform independent way
                // Build a list of browsers to try, in this order.
                String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
                        "netscape", "opera", "links", "lynx"};

                // Build a command string which looks like "browser1 "url" || browser2 "url" ||..."
                StringBuilder cmd = new StringBuilder();
                for (int i = 0; i < browsers.length; i++)
                    cmd.append(i == 0 ? "" : " || ").append(browsers[i]).append(" \"").append(url).append("\" ");

                rt.exec(new String[]{"sh", "-c", cmd.toString()});

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}