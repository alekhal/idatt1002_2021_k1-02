package edu.ntnu.idatt1002.k1g02;

import edu.ntnu.idatt1002.k1g02.dao.TaskDAO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Class that manages all the tasks in an ArrayList.
 * The class uses recycledIds to keep track of all previously used IDs so that we dont have to search through all the tasks
 * every time we need to add another one.
 */
public class TaskRegister {
    private final ArrayList<Task> TASK_LIST;
    private final ArrayList<Integer> RECYCLED_ID_LIST;
    private File saveFile;
    private final TaskDAO DAO;

    public TaskRegister() {
        TASK_LIST = new ArrayList<>();
        RECYCLED_ID_LIST = new ArrayList<>();
        try {
            saveFile = new File(System.getProperty("user.home"), "tasks.csv");
            saveFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DAO = new TaskDAO();
    }

    public ArrayList<Task> getTasks() {
        return new ArrayList<>(TASK_LIST);
    }

    public Task getTaskFromUniqueId(int id){
        return TASK_LIST.stream().filter(e -> e.getUNIQUE_ID() == id).collect(Collectors.toList()).get(0);
    }

    public ArrayList<Task> getUnfinishedTasks(){
        return (ArrayList<Task>) TASK_LIST.stream().filter(e -> !e.getIsFinished()).collect(Collectors.toList());
    }

    public ArrayList<Task> getFinishedTasks(){
        return (ArrayList<Task>) TASK_LIST.stream().filter(Task::getIsFinished).collect(Collectors.toList());
    }

    /**
     * Adds task WITH dueDate.
     * Checks recycledIds for any previously used uniqueId that is now available, if the list is empty
     * it generates a new ID equal to the length of the tasks list +1. If it uses an ID from the recycledIds list it
     * then removes it from the list to show that it is no longer available.
     * @param name of the task to be added.
     * @param description of the task to be added.
     * @param category of the task to be added.
     * @param dueDate of the task to be added.
     * @param priority of the task to be added.
     */
    public void addTask(String name, String description, String category, LocalDate dueDate, String priority, LocalDate startDate, boolean isFinished, LocalDate finishDate) {
        if (RECYCLED_ID_LIST.size() == 0) {
            TASK_LIST.add(new Task(name, description, category, dueDate, priority, startDate, isFinished, finishDate, TASK_LIST.size() + 1));
        } else {
            if (TASK_LIST.add(new Task(name, description, category, dueDate, priority, startDate, isFinished, finishDate, RECYCLED_ID_LIST.get(0)))) {
                RECYCLED_ID_LIST.remove(0);
            }
        }
    }

    /**
     * Removes a task using the uniqueId property. After it has removed the task it adds the uniqueId to the
     * recycledIds list.
     * @param uniqueId the unique ID of the task you want to remove. Is added back to the pool of unique ID's after.
     */
    public void removeTask(int uniqueId) {
        for (Task task : getTasks()) {
            if (task.getUNIQUE_ID() == uniqueId) {
                TASK_LIST.remove(task);
                RECYCLED_ID_LIST.add(uniqueId);
            }
        }
    }

    public File getSaveFile() {
        return saveFile;
    }

    public void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    /**
     * Loads task from CSV file. Adds them to the register.
     */
    public void load() {
        DAO.fillRegisterFromCSV(this);
    }

    /**
     * Save tasks to CSV file
     */
    public void save() {
        DAO.saveToCSV(this);
    }

    /**
     * Sorting the list of tasks by due date, using Comparator
     */
    public void sortByDate() {
        TASK_LIST.trimToSize();
        TASK_LIST.sort(Comparator.comparing(Task::getDueDate));
    }

    /**
     * Sorting the list of tasks by priority, using Comparator
     */
    public void sortByPriority() {
        TASK_LIST.trimToSize();
        TASK_LIST.sort(Comparator.comparing(Task::getPriority));
    }

    /**
     * Sorting the list of tasks by category, using Comparator
     */
    public void sortByCategory() {
        TASK_LIST.trimToSize();
        TASK_LIST.sort(Comparator.comparing(Task::getCategory));
    }
}