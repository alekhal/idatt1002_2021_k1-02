package edu.ntnu.idatt1002.k1g02;

import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class TaskTest {
    Task testTask1;
    Task testTask2;
    Task testTask3;

    @BeforeEach
    public void setup() {
        testTask1 = new Task(
                "Test task",
                "Task is created for testing purposes",
                "Testing",
                LocalDate.now().plusDays(1),
                "!!",
                LocalDate.now(),
                false,
                null,
                1);

        testTask2 = new Task(
                "Test task",
                "Task is created for testing purposes",
                "Testing",
                LocalDate.now().plusDays(1),
                "!!",
                LocalDate.now(),
                false,
                null,
                1);

        testTask3 = new Task(
                "Test task 3!!",
                "Task is created, but why?",
                "Something else",
                LocalDate.now().plusDays(10),
                "!",
                LocalDate.now(),
                false,
                null,
                2);
    }

    @Test
    public void correctStringFromConvertToCSV() {
        String dueDate = LocalDate.now().plusDays(1).toString();
        String startDate = LocalDate.now().toString();
        assertEquals("Test task;" + dueDate + ";Task is created for testing purposes;!!;Testing;false;" + startDate + ";null", testTask1.convertToCSV());
    }

    @Test
    public void testGetAsHBoxGivesRightValuesToChildren() {
        HBox hbox = testTask1.getAsHBox();
        String priority = ((Text) ((HBox) hbox.getChildren().get(0)).getChildren().get(0)).getText();
        String name = ((Text) ((HBox) hbox.getChildren().get(1)).getChildren().get(0)).getText();
        String dueDate = ((Text) ((HBox) hbox.getChildren().get(3)).getChildren().get(0)).getText();
        String category = ((Text) ((HBox) hbox.getChildren().get(5)).getChildren().get(0)).getText();
        Assertions.assertAll(
                () -> assertEquals("!!", priority),
                () -> assertEquals("Test task", name),
                () -> assertEquals("Due: " + LocalDate.now().plusDays(1), dueDate),
                () -> assertEquals("Category: TESTING", category)
        );
    }

    @Nested
    public class finishAndUnfinishTests {
        @Test
        public void checkThatFinishTaskUpdatesValuesCorrectly() {
            testTask1.setIsFinished(false);
            testTask1.setFinishDate(null);
            testTask1.finishTask();
            Assertions.assertAll(
                    () -> assertTrue(testTask1.isFinished),
                    () -> assertEquals(LocalDate.now(), testTask1.finishDate)
            );
        }

        @Test
        public void checkThatNotFinishedUpdatesValuesCorrectly() {
            testTask1.setIsFinished(true);
            testTask1.setFinishDate(LocalDate.now());
            testTask1.notFinished();
            Assertions.assertAll(
                    () -> assertFalse(testTask1.isFinished),
                    () -> assertNull(testTask1.finishDate)
            );
        }
    }

    @Nested
    @DisplayName("Tests for the .equals-method")
    public class equalsTests {
        @Nested
        public class positiveTestsForEquals {
            @Test
            public void testEqualsSameTaskTwice() {
                assertEquals(testTask1, testTask1);
            }

            @Test
            public void testEqualsDifferentTasksButSameData() {
                assertEquals(testTask1, testTask2);
            }
        }

        @Nested
        public class negativeTestsForEquals {
            @Test
            public void testEqualsDifferentTasksDifferentData() {
                assertNotEquals(testTask1, testTask3);
            }
        }
    }
}