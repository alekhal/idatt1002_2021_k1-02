package edu.ntnu.idatt1002.k1g02.dao;

import edu.ntnu.idatt1002.k1g02.TaskRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskDAOTest {

    private TaskRegister taskRegister;

    @BeforeEach
    public void setUp() {
        this.taskRegister = new TaskRegister();
        taskRegister.setSaveFile(new File("src/test/resources/TestData.csv"));
        try (OutputStream os = new FileOutputStream("src/test/resources/TestData.csv");
             PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            System.out.println("file clear");
        } catch (IOException e) {
            e.printStackTrace();
        }
        taskRegister.load();
        // Using for loop to get the same test data every time
        String[] priorities = {"!", "!!", "!!!"};
        String[] categoriesList = {"School", "Work", "Play"};
        boolean isFinished = false;
        LocalDate finishDate = null;
        for (int i = 0; i < 20; i++) {
            String name = "Test " + i;
            String description = "Lorem ipsum dolor sit amet nr. " + i;
            String category = categoriesList[i % 3];
            LocalDate dueDate = LocalDate.now().plusDays((i % 4) - 1);
            String priority = priorities[i % 3];
            LocalDate startDate = LocalDate.now().minusDays(i);
            if (i == 10) { // Sets getIsFinished to true at i == 10
                isFinished = true;
                finishDate = LocalDate.now();
            }
            taskRegister.addTask(name, description, category, dueDate, priority, startDate, isFinished, finishDate);
        }
        taskRegister.save();
    }

    @Test
    public void fillRegisterFromCSV() {
        taskRegister = new TaskRegister();
        taskRegister.setSaveFile(new File("src/test/resources/TestData.csv"));
        taskRegister.load();
        assertEquals("Test 10", taskRegister.getTasks().get(10).getName());
    }

    @Test
    public void saveToCSVTest() {
        int nrOfLines = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("src/test/resources/TestData.csv"))) {
            while (br.readLine() != null) {
                nrOfLines++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(20, nrOfLines);
    }
}