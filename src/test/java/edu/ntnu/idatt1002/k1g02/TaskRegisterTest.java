package edu.ntnu.idatt1002.k1g02;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TaskRegisterTest {
    private TaskRegister taskRegister;

    @BeforeEach
    public void setUp() {
        this.taskRegister = new TaskRegister();
        // Using for loop to get the same test data every time
        // A .csv-file would be edited by the tests
        String[] priorities = {"!", "!!", "!!!"};
        String[] categoriesList = {"School", "Work", "Play"};
        boolean isFinished = false;
        LocalDate finishDate = null;
        for (int i = 0; i < 20; i++) {
            String name = "Test " + i;
            String description = "Lorem ipsum dolor sit amet nr. " + i;
            String category = categoriesList[i % 3];
            LocalDate dueDate = LocalDate.now().plusDays((i % 4) - 1);
            String priority = priorities[i % 3];
            LocalDate startDate = LocalDate.now().minusDays(i);
            if (i == 10) { // Sets getIsFinished to true at i == 10
                isFinished = true;
                finishDate = LocalDate.now();
            }
            taskRegister.addTask(name, description, category, dueDate, priority, startDate, isFinished, finishDate);
        }
    }


    @Test
    public void getTaskFromUniqueIdTest() {
        System.out.println(taskRegister.getTasks().get(0).getUNIQUE_ID());
        assertEquals("Test 0", taskRegister.getTaskFromUniqueId(1).getName());
    }

    @Test
    public void getUnfinishedTasksTest() {
        assertEquals(10, taskRegister.getUnfinishedTasks().size());
    }

    @Test
    public void getFinishedTasksTest() {
        assertEquals(10, taskRegister.getFinishedTasks().size());
    }

    @Test
    public void removeTaskTest() {
        assertEquals(20, taskRegister.getTasks().size());
        taskRegister.removeTask(1);
        assertEquals(19, taskRegister.getTasks().size());
    }

    @Test
    public void setSaveFileTest(){
        taskRegister.setSaveFile(null);
        assertThrows(NullPointerException.class, () -> taskRegister.load());
    }


    /**
     * We use Comparator to sort the list after different criteria. Since Comparator is already heavily tested, We will only do
     * light testing on the methods here.
     */

    @Test
    public void sortByDateTest() {
        taskRegister.sortByDate();
        assertEquals(LocalDate.now().plusDays(2), taskRegister.getTasks().get(19).getDueDate());
    }

    @Test
    public void sortByCategoryTest() {
        taskRegister.sortByCategory();
        assertEquals("Play", taskRegister.getTasks().get(2).getCategory());
    }

    @Test
    public void sortByPriorityTest() {
        taskRegister.sortByPriority();
        assertEquals("!!!", taskRegister.getTasks().get(19).getPriority());
    }
}