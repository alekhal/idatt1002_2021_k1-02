# Get started
This page will help you getting started using the TODO application.

# TODO application
TODO is a simple, easy-to-use application for managing todo-lists.
- Tasks can be given *priority*, a *due date* and a *category*.
- Lists are **saved locally** in .csv-format for easy export and import.
- Lists are **sortable** after priority, date or category.

# Table of content
- [TODO](#todo-application)
- [Table of content](#table-of-content)
- [Installation](#installation)
  - [Prerequesites](#prerequesites)
  - [For regular users](#for-regular-users)
  - [For developers](#for-developers)
- [Help](#help)
- [Authors](#authors)

# Installation
See [installation](https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02/-/wikis/System/Installation-manual).

# Prerequesites
* JRE 11 or higher.

# For regular users
Download [this](https://drive.google.com/file/d/1aM0NVpFsjq6CHu6VN1fS8R2HSbvhYuNa/view?usp=sharing) jar file and run it on your computer. If JRE 11 or higher is not installed, you'll have to do so following the 
description under [installation](https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02/-/wikis/System/Installation-manual).

# For developers
Clone the project and open it in your preferred IDE. To clone the project, just use one of the following commands.
### Using ssh:
`git clone git@gitlab.stud.idi.ntnu.no:alekhal/idatt1002_2021_k1-02.git`

### Using https:
`git clone https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02.git`

### Changing the pom depending on your OS:
You should remove the dependencies not related to your operating system before trying to compile. These are located  between line 23 and 40 in the pom.xml.

# Help
Stuck or need help with a feature? Here is our [user guide](https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02/-/wikis/System/User-manual).
Interested in developing the app further? Here is our [wiki](https://gitlab.stud.idi.ntnu.no/alekhal/idatt1002_2021_k1-02/-/wikis/home) with all of our documentation.

# Authors (Group K1-2)
| Person | Role |
| ------ | ------ |
| Aune, Morten | Documentation/development |
| Bakken, Tor-Øyvind P. | Secretary/developer |
| Hansen, Erik B. | QA/developer |
| Holthe, Aleksander H. | Group/project manager |

